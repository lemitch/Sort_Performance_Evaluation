# Sort_Performance_Evaluation

A project to evaluate performance of the Insertion and Quick Sort algorithms on datasets of varying size.

##### Introduction

The goal of this project was to compare the performance of two sorting algorithms and determine any relationships between dataset size and algorithm speed.

This was accomplished by writing a program in C++ that could generate randomized datasets then sort them using the two algorithms and print the results to an output file.

##### Data Generation
The datasets are of sizes 1000, 5000, and 50000. The data is comprised of random integers between 1 and the size of the dataset. Meaning that each data point in a set of size 5000 will be between 1 and 5000. 10 trials of each size are generated for a total of 30 sets.

##### Data Sorting
To sort the data, the program opens a data file and loads it into a vector line by line. The program then starts a CPU clock, runs the sort function, then stops the clock. The sort function returns the number of comparisons made for that sort. Finally, the program prints a line of output to the screen and the output file in the following form:

**{Sort type},{Dataset #1-10},{Dataset size},{# comparisons},{CPU time},{Worst case # comparisons}**

For example, a Quick Sort on the 5th dataset of size 5000 might print:

`Q,5,5000,46469,0.000916,56809`

##### Running the Program
To run the program, open the program directory in a terminal capable of compiling C++ code. Compile the program and include either the character "D" or "S" as a parameter; "D" to generate the datasets and "S" to sort them.

Run the executable file and the program will output the results to the terminal as well as a text file ('output.txt').

##### Results
The Quick Sort algorithm was exponentially better than the Insertion Sort. This is especially apparent when comparing the datasets of size 50000:

*Insertion vs Quick Sort Times:*  
Insertion average:	5.9594958s  
Quick average:	0.0107305s  

The Insertion Sort took over 500x as long as the Quick Sort. This can be explained by the sheer number of comparisons the Insertion Sort made, which is almost 1000x as many as the Quick Sort:

*Insertion vs Quick Sort Comparisons:*  
Insertion average:	624,609,390.3  
Quick average:	648,920  

We know that the average number of comparisons for Insertion Sort is O(n2) whereas the average number of comparisons for Quick Sort is O(nlog(n)), making the Quick Sort much more practically useful. They were both able to complete the two smaller dataset sizes in < 1 second which goes to show that as the datasets get larger, it become exponentially more efficient to use Quick Sort.

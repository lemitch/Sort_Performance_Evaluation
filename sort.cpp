#include <iostream>
#include <string>
#include <ctime>
#include <time.h>
#include <cstdlib>
#include <math.h>
#include <stdio.h>
#include <ctype.h>
#include <fstream>
#include <vector>
#include <iomanip>

using namespace std;

/*
	randNum() returns a random number between 1 and {size}.
	If called with incorrect {size} returns with error.
*/
int randNum(int size)
{

	if (size == 1000) {

		// seed rand with time in milliseconds
		srand (clock());

		return rand() % 1000 + 1;

	} else if (size == 5000) {

		// seed rand with time in milliseconds
		srand (clock());

		return rand() % 5000 + 1;

	} else if (size == 50000) {

		// seed rand with time in milliseconds
		srand (clock());

		return rand() % 50000 + 1;

	} else {

		cout << "ERROR: Incorrect dataset size\n";
		return -1;

	}

}

/*
	performs an insertion sort by reference on a vector<int> parameter,
	returns # comparisons made during sort
*/
int insertionSort(vector<int> &vec) {

	int i, key, j;
	int comparisons = 0;

	for (i=1; i < vec.size(); i++) {

		key = vec[i];
		j = i-1;

		while (j >= 0 && vec[j] > key) {
			vec[j+1] = vec[j];
			j = j - 1;
			comparisons++;
		}

		vec[j+1] = key;

	}

	return comparisons;
}
/*
	global var used to temporarily hold total comparisons made in the
	quickSort() function
*/
int qsComparisons;

/*
	utility function for quickSort().
	handles partitioning the vector,
	returns the new index
*/
int partition(vector<int> &vec, int left, int right) {

	int i = left, j = right;
	int temp;
	int pivot = vec[(left + right) / 2];

	while (i <= j) {

		while (vec[i] < pivot){
			i++;
			qsComparisons++;
		}

		while (vec[j] > pivot){
			j--;
			qsComparisons++;
		}

		if (i <= j) {

			temp = vec[i];
			vec[i] = vec[j];
			vec[j] = temp;
			i++;
			j--;

		}

	}

	return i;

}

/*
	performs a quick sort by reference on a vector<int> parameter,
	uses utility function partition()
*/
void quickSort(vector<int> &vec, int left, int right) {

	int index = partition(vec, left, right);
	if (left < index - 1)
		quickSort(vec, left, index - 1);
	if (index < right)
		quickSort(vec, index, right);

	return;

}

/*
	handles the command-line input:
		'D' = Generate datasets for all sizes
		'S' = perform Insertion Sort and Quick Sort on the datasets
			and print results to "output.txt"
	results displayed as:
		{Sort type},
		{dataset #1-10},
		{# comparisons},
		{cpu time (seconds)},
		{worst-case comparisons}
	example:
		Q,7,5000,46213,0.000863,56809
*/
int main(int argc, char *argv[])
{

	/*
		If command line option 'D' is called, generate datasets.
		3 sizes: 1,000; 5,000; 50,000.
		Each size has 10 sets.
		Each set is stored in its own file.
		All 10 files of each size are stored in their own directory.
	*/
	if (toupper(*argv[1]) == 'D') {

		ofstream file;

		// SIZE 1000
		for (int j = 1; j < 11; j++) {

			string filename = "1000/data1000_" + to_string(j) + ".txt";
			file.open(filename);

			for (int i=0; i < 1000; i++)
				file << randNum(1000) << "\n";

			file.close();

		}

		// SIZE 5000
		for (int j = 1; j < 11; j++) {

			string filename = "5000/data5000_" + to_string(j) + ".txt";
			file.open(filename);

			for (int i=0; i < 5000; i++)
				file << randNum(5000) << "\n";

			file.close();

		}

		// SIZE 50000
		for (int j = 1; j < 11; j++) {

			string filename = "50000/data50000_" + to_string(j) + ".txt";
			file.open(filename);

			for (int i=0; i < 50000; i++)
				file << randNum(50000) << "\n";

			file.close();

		}

	}

	/*
		If command line option 'S' is called, sort datasets.
		Insertion sort, then quicksort
		Print results for each dataset to command-line and "output.txt"
	*/
	else if (toupper(*argv[1] == 'S')) {

		// set decimal precision
		cout << fixed << setprecision(12);

		ofstream outFile;
		outFile.open("output.txt");

		//////////////////////////////
		//      INSERTION SORT      //
		//////////////////////////////

		// Size = 1,000
		for (int j=1; j < 11; j++) {

			// Declare vector, copy dataset from file to vector
			vector <int> vec;
			string line;
			int comparisons;

			ifstream file;
			file.open("1000/data1000_" + to_string(j) + ".txt");

			while (getline(file, line)) {
				vec.push_back(stoi(line));
			}
			
			file.close();


			double start = clock();

			comparisons = insertionSort(vec);

			double end = clock();


			double sortTime = (end - start) / CLOCKS_PER_SEC;
			
			cout << "I," + to_string(j) + ",1000," + to_string(comparisons) + "," + to_string(sortTime) + ",499500\n\n";

			outFile << "I," + to_string(j) + ",1000," + to_string(comparisons) + "," + to_string(sortTime) + ",499500\n";



			// for testing
			/*
			for (int i=0; i < vec.size(); i++) {
				cout << vec[i] << "\n";
			}

			cout << "\n\n";
			*/

		}

		// Size = 5,000
		for (int j=1; j < 11; j++) {

			// Declare vector, copy dataset from file to vector
			vector <int> vec;
			string line;
			int comparisons;

			ifstream file;
			file.open("5000/data5000_" + to_string(j) + ".txt");

			while (getline(file, line)) {
				vec.push_back(stoi(line));
			}
			
			file.close();


			double start = clock();

			comparisons = insertionSort(vec);

			double end = clock();


			double sortTime = (end - start) / CLOCKS_PER_SEC;
			
			cout << "I," + to_string(j) + ",5000," + to_string(comparisons) + "," + to_string(sortTime) + ",12497500\n\n";

			outFile << "I," + to_string(j) + ",5000," + to_string(comparisons) + "," + to_string(sortTime) + ",12497500\n";


		}

		// Size = 50,000
		for (int j=1; j < 11; j++) {

			// Declare vector, copy dataset from file to vector
			vector <int> vec;
			string line;
			int comparisons;

			ifstream file;
			file.open("50000/data50000_" + to_string(j) + ".txt");

			while (getline(file, line)) {
				vec.push_back(stoi(line));
			}
			
			file.close();


			double start = clock();

			comparisons = insertionSort(vec);

			double end = clock();


			double sortTime = (end - start) / CLOCKS_PER_SEC;
			
			cout << "I," + to_string(j) + ",50000," + to_string(comparisons) + "," + to_string(sortTime) + ",1250025000\n\n";

			outFile << "I," + to_string(j) + ",50000," + to_string(comparisons) + "," + to_string(sortTime) + ",1250025000\n";
		}


		//////////////////////////////
		//        QUICK SORT        //
		//////////////////////////////

		// Size = 1,000
		for (int j=1; j < 11; j++) {

			// Declare vector, copy dataset from file to vector
			vector <int> vec;
			string line;
			int comparisons;
			qsComparisons = 0;

			ifstream file;
			file.open("1000/data1000_" + to_string(j) + ".txt");

			while (getline(file, line)) {
				vec.push_back(stoi(line));
			}

			file.close();


			double start = clock();

			quickSort(vec, 0, vec.size() - 1);

			double end = clock();

			// get comparisons from global var "qsComparisons"
			comparisons = qsComparisons;
			// reset "qsComparisons" to 0
			qsComparisons = 0;



			double sortTime = (end - start) / CLOCKS_PER_SEC;
			
			cout << "Q," + to_string(j) + ",1000," + to_string(comparisons) + "," + to_string(sortTime) + ",8977\n\n";

			outFile << "Q," + to_string(j) + ",1000," + to_string(comparisons) + "," + to_string(sortTime) + ",8977\n";



			// for testing
			/*
			for (int i=0; i < vec.size(); i++) {
				cout << vec[i] << "\n";
			}

			cout << "\n\n";
			*/

		}

		// Size = 5,000
		for (int j=1; j < 11; j++) {

			// Declare vector, copy dataset from file to vector
			vector <int> vec;
			string line;
			int comparisons;
			qsComparisons = 0;

			ifstream file;
			file.open("5000/data5000_" + to_string(j) + ".txt");

			while (getline(file, line)) {
				vec.push_back(stoi(line));
			}
	
			file.close();


			double start = clock();

			quickSort(vec, 0, vec.size() - 1);

			double end = clock();
			
			// get comparisons from global var "qsComparisons"
			comparisons = qsComparisons;
			// reset "qsComparisons" to 0
			qsComparisons = 0;



			double sortTime = (end - start) / CLOCKS_PER_SEC;
			
			cout << "Q," + to_string(j) + ",5000," + to_string(comparisons) + "," + to_string(sortTime) + ",56809\n\n";

			outFile << "Q," + to_string(j) + ",5000," + to_string(comparisons) + "," + to_string(sortTime) + ",56809\n";


		}

		// Size = 50,000
		for (int j=1; j < 11; j++) {

			// Declare vector, copy dataset from file to vector
			vector <int> vec;
			string line;
			int comparisons;
			qsComparisons = 0;

			ifstream file;
			file.open("50000/data50000_" + to_string(j) + ".txt");

			while (getline(file, line)) {
				vec.push_back(stoi(line));
			}

			file.close();


			double start = clock();

			quickSort(vec, 0, vec.size() - 1);

			double end = clock();
			
			// get comparisons from global var "qsComparisons"
			comparisons = qsComparisons;
			// reset "qsComparisons" to 0
			qsComparisons = 0;



			double sortTime = (end - start) / CLOCKS_PER_SEC;
			
			cout << "Q," + to_string(j) + ",50000," + to_string(comparisons) + "," + to_string(sortTime) + ",734465\n\n";

			outFile << "Q," + to_string(j) + ",50000," + to_string(comparisons) + "," + to_string(sortTime) + ",734465\n";
		}

		outFile.close();

	}

	/*
		Handle Error
	*/
	else {

		cout << "ERROR: Call D to generate datasets or call S to sort the most recently generated\n";

	}

	return 0;
}